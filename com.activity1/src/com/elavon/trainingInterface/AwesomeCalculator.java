package com.elavon.trainingInterface;

public interface AwesomeCalculator {

	int getSum(int augend, int addend);
	int getSum(int... summands);
	double getDifference(double minuend, double subtrahend);
	double getProduct(double multiplicand, double multiplier);
	double getProduct(double... factors);
	String getQuotientAndRemainder(double dividend, double divisor);
	double toCelsius(double fahrenheit);
	double toFahrenheit(double celsius);
	double toKilogram(double lbs);
	double toPound(double kg);
	boolean isPalindrome(String str);

}
