package com.elavon.recursion;

public class Recursion {

	public static void main(String[] args)
	{
		int n = 5;
		System.out.println(recursion(n));
	}

	static int recursion(int n)
	{
		if(n>1)
		 return n * recursion(n-1); 	
		return n;
	
	}
	
}
