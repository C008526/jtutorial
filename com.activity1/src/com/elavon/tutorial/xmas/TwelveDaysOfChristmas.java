package com.elavon.tutorial.xmas;

public class TwelveDaysOfChristmas
{
public static void main(String[] args)
{
	TwelveDaysOfChristmas.printLyrics();
}

public static void printLyrics()
{
//variable that determines day of Xmas and number of total gifts for that day
	int sing_count =1;
//variable that determines what gift to give on loop of song
	int gift_counter=1;
//String for xmas day
	String xmas_day ="";
//String for xmas gift to be given
	String xmas_gift ="";
//Variable to determine if last line of song should have "and"
	Boolean has_and_word=false;
	
	System.out.println("***12 Days of Christmas***\n***Using Switch Case, Do While, While and If Statement***\n");

//Song continues to loop for 12 times	
	while (sing_count < 13)
		{
			  switch (sing_count)
			   {
				case 1: xmas_day = "1st";
						break;
				case 2: xmas_day = "2nd";
						break;
				case 3: xmas_day = "3rd";
						break;
				case 4: xmas_day = "4th";
						break;
				case 5: xmas_day = "5th";
						break;
				case 6: xmas_day = "6th";
						break;
				case 7: xmas_day = "7th";
						break;
				case 8: xmas_day = "8th";
						break;
				case 9: xmas_day = "9th";
						break;
				case 10: xmas_day = "10th";
						break;
				case 11: xmas_day = "11th";
						break;
				case 12: xmas_day = "12th";
						break;
				default:
						break;
			   }
				
	System.out.println("On the "+ xmas_day + " day of Christmas my true love sent to me");
			do
			   {	
			//Is gift is more than one?
				if(sing_count < 13 && sing_count > 1)
					has_and_word=true;
			//This is the last Gift to Sing? if yes then Add "and"
				if(has_and_word==true && gift_counter == 1)
					System.out.print("and ");
			//Gifts received
				if(gift_counter == 1)
					xmas_gift = "a Partridge in a Pear Tree";
				else if(gift_counter == 2)
					xmas_gift = "Two Turtle Doves";
				else if(gift_counter == 3)
					xmas_gift = "Three French Hens";
				else if(gift_counter == 4)
					xmas_gift = "Four Calling Birds";
				else if(gift_counter == 5)
					xmas_gift = "Five Golden Rings";
				else if(gift_counter == 6)
					xmas_gift = "Six Geese a Laying";
				else if(gift_counter == 7)
					xmas_gift = "Seven Swans a Swimming";
				else if(gift_counter == 8)
					xmas_gift = "Eight Maids a Milking";
				else if(gift_counter == 9)
					xmas_gift = "Nine Ladies Dancing";
				else if(gift_counter == 10)
					xmas_gift = "Ten Lords a Leaping";
				else if(gift_counter == 11)
					xmas_gift = "Eleven Pipers Piping";
				else if(gift_counter == 12)
					xmas_gift = "Twelve Drummers Drumming";
			
	System.out.println(xmas_gift);	
				
				gift_counter--;
			} while (gift_counter != 0);//continues to loop until last gift
		//Adds 1 day to counter	
		sing_count++;
		gift_counter = sing_count;
		//resets the value in loop
		has_and_word =false;
		
		System.out.println();		
	} 

}

}
