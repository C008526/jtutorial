package com.elavon.activity2;

//Create a NetworkConnectionImpl class and implement the Network Connection Class

public class NetworkConnectionImpl implements NetworkConnection
{
	String network;
	
	@Override
	public String connect(String testNetwork)
	{
		network = testNetwork;
		return testNetwork;
	}

	@Override
	public Boolean connectStatus()
	{
		
		Boolean checkNetwork = false;
		if(network != null)
			{
			System.out.println("There is an Established Network Connection yey! :D \nConnection Name: "+network);
			checkNetwork = true;
			return checkNetwork;
			}
		else
			{
			System.out.println("No Connection :( Try again");
			checkNetwork = false;
			return checkNetwork;
			}
	}

	public static void main(String[] args)
	{
		NetworkConnectionImpl newNetwork = new NetworkConnectionImpl();
		newNetwork.connect("Test Connection");
		newNetwork.connectStatus();
	}
}
