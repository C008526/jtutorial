package com.elavon.activity2;


//Create an Interface Connection with connect and connectStatus method
public interface NetworkConnection
{
//takes a string name and returns an established network connection	
	public String connect (String testNetwork);
//returns true if a connection is established
	public Boolean connectStatus();
}

//Create a smartTV class that inherits from coloredTV

