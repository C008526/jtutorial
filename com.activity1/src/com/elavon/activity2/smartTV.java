package com.elavon.activity2;


//Create a smart TV class that inherits from ColoredTV
public class smartTV extends ColoredTV
{
	public static String network;
	public static Boolean networkAccess=false;
	
	public smartTV(String tvModel, String tvBrand)
	{	
		this.tvBrand = tvBrand; 
		this.tvModel = tvModel;
	}

//Takes a String that serves as network name
	public void connectToNetwork(String network)
	{
	}
//Returns the connection status of network
	public Boolean hasNetworkConnection(String testnetwork)
	{
		if(testnetwork != null)
			networkAccess = true;
		return networkAccess;
	}
			

//Create a main method on the smart tv class
	public static void main(String[] args)
	{
//Instantiate a samsungTV object of smartTV with values
		smartTV samsungTV = new smartTV("SAMSUNG","SMTV1");
		
//Print the object to see default values of the properties
		System.out.println(samsungTV);
		
//Call the mute method on the samsung object
		samsungTV.tvMute();

//Call BrightnessUP 3 times
		int loop=0;
		while(loop < 3)
		{
		samsungTV.brightnessUp();
		loop++;
		}
// Call Brightness Down 5 times
		loop=0;
		while(loop < 5)
		{
		samsungTV.brightnessDown();
		loop++;
		}
//Call Contrast Up 3 times
		loop=0;
		while(loop < 3)
		{
		samsungTV.contrastUp();
		loop++;
		}
//Call Contrast Down 5 times
		loop=0;
		while(loop < 5)
		{
		samsungTV.contrastDown();
		loop++;
		}

//Call Picture Up 3 times		
		loop=0;
		while(loop < 3)
		{
		samsungTV.pictureUp();
		loop++;
		}
//Call Picture Down 5 times - no value. assumed it was 5 because of pattern in powerpoint
		loop=0;
		while(loop < 5)
		{
		samsungTV.pictureDown();
		loop++;
		}
//Call Channel Up 3 times
		
		loop=0;
		while(loop < 2)
		{
		samsungTV.channelUp();
		loop++;
		}
		
//Print the object to see the adjusted properties
		System.out.println(samsungTV);		
		
		
//Call connectToNetwork passing a String
		samsungTV.connectToNetwork("Test Network");
//What happened?
//		--No display output. We successfully passed a String which will indicate that there is a network connection
//		But it is not the method that will display confirm if there is a network connection.
//		If we will be implementing an interface then we will force the 2 existing methods to be used.
		
	}
}
