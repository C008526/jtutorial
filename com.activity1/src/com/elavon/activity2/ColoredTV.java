package com.elavon.activity2;

import java.util.*;
import java.lang.*;


//Create a TV class that extends the capabilities of TV with new properties
public class ColoredTV extends TV
{

	int tvBrightness;
	int tvContrast;
	int tvPicture;

//All the properties are defaulted to 50 when instantiated
	public ColoredTV(String tvModel, String tvBrand)
	{
		this.tvBrand = tvBrand; 
		this.tvModel = tvModel;
		tvBrightness=50;
		tvContrast=50;
		tvPicture=50;
	}
	
	public ColoredTV()
	{	
		//default constructor for Child Classes of SmartTV to avoid error
		tvBrightness=50;
		tvContrast=50;
		tvPicture=50;
	}
	
//Colored TV will have new methods
	public void brightnessUp()
	{
		tvBrightness++;
	}

	public void brightnessDown()
	{
		tvBrightness--;
	}	

	public void contrastUp()
	{
		tvContrast++;
	}	

	public void contrastDown()
	{
		tvContrast--;
	}
	
	public void pictureUp()
	{
		tvPicture++;
	}	

	public void pictureDown()
	{
		tvPicture--;
	}		

	
//Accepts an integer to switch to a desired channel
	public void switchToChannel()
	{
		System.out.println("Input Channel: ");
		Scanner scan = new Scanner(System.in);
		int channel = scan.nextInt();
		
		if (tvChannel == channel)
			System.out.println("Channel is the same! "+channel);
		
		else if (channel > -1 && channel <14  )
		{
			tvChannel = channel;
			System.out.println("Channel is changed to "+channel);
		}
		else
			System.out.println("Valid Channels are only 0-13");
		
	}
	
	public void tvMute()
	{
		tvVolume=0;
	}

//Append the new properties to the previous format
	public String toString()
	{
		return super.toString()+"\n[Brightness: "+tvBrightness+", Contrast: "+tvContrast+", Picture: "+tvPicture+"]";
	}


//Create a main method on the colored tv class to test the new capabilities of colored TV	
	public static void main(String[] args)
	{
		
//Instantiate bnwTV and sonyTV objects from TV Class
//Assign Specified Values
		TV bnwTV = new TV("Admiral","A1");
		TV sonyTV = new TV("Sony","S1");
		
		
//Print the objects. 
		System.out.println(bnwTV);
		System.out.println(sonyTV);
/*What can be observed from the output?
		--Since these objects are instance of the parent class,
		  only the toString() from the parent class it displayed.
		  It will not append the newly added values on the Colored TV for toString() */

		
/*Try calling the brightnessUp of the TV. What happened?
		
		sonyTV.brightnessUp();		
 	
  --sonyTV is an instance of TV which is the parent entity, parent entity does not inherit from child */
		
		
//Instantiate objects of Colored TV Child Class
		ColoredTV sharpTV = new ColoredTV("SHARP","S1");


//Call the mute method on sharp tv. Value of Volume will be 0 from 5
		sharpTV.tvMute();
		System.out.println(sharpTV.toString());	
		
		
//Adjust the brightness, contrast and picture using the new methods
//Final Values will be Brightness = 75, Contrast = 75, Picture = 75
		int loop=0;
		while(loop < 50)
		{
		sharpTV.brightnessUp();
		loop++;
		}

		loop=0;
		while(loop < 25)
		{
		sharpTV.brightnessDown();
		loop++;
		}
		
		loop=0;
		while(loop < 50)
		{
		sharpTV.contrastUp();
		loop++;
		}
		
		loop=0;
		while(loop < 25)
		{
		sharpTV.contrastDown();
		loop++;
		}
		
		loop=0;
		while(loop < 50)
		{
		sharpTV.pictureUp();
		loop++;
		}
		
		loop=0;
		while(loop < 25)
		{
		sharpTV.pictureDown();
		loop++;
		}

//Print the object sharp tv to check the adjusted properties
		System.out.println(sharpTV.toString());	

	}

}
