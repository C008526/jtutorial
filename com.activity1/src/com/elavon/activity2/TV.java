package com.elavon.activity2;

import java.lang.*;
import java.util.*;

import org.omg.Messaging.SyncScopeHelper;

//Create TV Class with protected attributes
public class TV
{
	
// Create a TV Class with the following protected attributes
	protected String tvBrand;
	protected String tvModel;
	protected Boolean tvPowerON;
	protected int tvChannel;
	protected int tvVolume;

//Create a constructor taking the tvs brand and model and set others default attributes 
	public TV (String tvBrand, String tvModel)
	{
		this.tvBrand = tvBrand; 
		this.tvModel = tvModel;
		tvPowerON = false;
		tvChannel = 0;
		tvVolume = 5;
	}
	
	public TV()
	{
		//default constructor for Child Classes of Colored TV to avoid error
		tvPowerON = false;
		tvChannel = 0;
		tvVolume = 5;		
	}

//Declare Methods
	public void turnOn()
	{
		tvPowerON = true;
	}
	
	public void turnOff()
	{
		tvPowerON = false;
	}
	
	public void channelUp()
	{
		tvChannel++;
	}
	
	public void channelDown()
	{
		tvChannel--;
	}
	
	public void volumeUp()
	{
		tvVolume++;
	}

	public void volumeDown()
	{
		tvVolume--;
	}
	
//declare toString method that returns the values of brand, model, power, channel and volume
//this method is called whenever any object is passed in syso
	public String toString()
	{
		return "TV Brand: "+tvBrand+"\nTV Model: "+tvModel+"\n[Power On: "+tvPowerON+", Channel: "+tvChannel+", Volume: "+tvVolume+"]";
	}

//Just an additional method -jah
	public String instanceOfClass()
	{
		return "Package Name: " + getClass().getPackage().getName() +"\nInstance of: " + getClass().getSimpleName();
	}
	
	public static void main (String args[])
	{
		
//Instantiate a TV Object with a brand Andre Electronics and Model One
	TV wowBagongTV = new TV("Andre Electronics", "One");
	
//Print the object using syso, check values are correct, toString() must be evident
	System.out.println(wowBagongTV.toString());
	
//Call the turnOn method of the object created
	wowBagongTV.turnOn();

//Switch the channel up 5 times
	int loop=0;
	while(loop < 5)
	{
	wowBagongTV.channelUp();
	loop++;
	}
	
//Switch the channel down once
	wowBagongTV.channelDown();

//Turn the volume down three times
	loop=0;
	while(loop < 3)
	{
	wowBagongTV.volumeDown();
	loop++;
	}	
	
//Turn the volume up once
	wowBagongTV.volumeUp();

//Turn Off the TV Object
	wowBagongTV.turnOff();

//Print the object and output should be TV is off, channel is 4 and volume is 3
	System.out.println(wowBagongTV.toString());
	}
	

}