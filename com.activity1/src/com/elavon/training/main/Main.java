package com.elavon.training.main;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import com.elavon.training.impl.ImplementingClass;

public class Main extends ImplementingClass {

	public static void main(String[] args)
	{
	
		ImplementingClass testMethods = new ImplementingClass();

		//All double values are rounded / formatted to 4 decimals when answers exceeds 4 decimal
		NumberFormat formatter = new DecimalFormat("#.####");
		
		//Array passed for varargs
		int[] addNumbers = {1,2,3,4,5};
		double[] multiplyNumbers = {1.5,1.5,2.5,7.5};
		System.out.println(testMethods.getSum(2, 1)+"\n"); 
		System.out.println(testMethods.getSum(addNumbers)+"\n");
		System.out.println(testMethods.getDifference(2, 1)+"\n");
		System.out.println(testMethods.getProduct(5, 5)+"\n"); 
		System.out.println(testMethods.getProduct(multiplyNumbers)+"\n"); 
		System.out.println(testMethods.getQuotientAndRemainder(8.5, 3)+"\n");
		System.out.println(formatter.format(testMethods.toCelsius(400))+"\u00b0C"+"\n"); 
		System.out.println(formatter.format(testMethods.toFahrenheit(7))+ "F\u00b0"+"\n"); 
		System.out.println(formatter.format(testMethods.toKilogram(60))+" kg"+"\n");
		System.out.println(formatter.format(testMethods.toPound(60))+" lbs"+"\n");	
		System.out.println(testMethods.isPalindrome("Level")+"\n");
		

		
		
	}

}
