package com.elavon.training.impl;
import com.elavon.trainingInterface.AwesomeCalculator;

public class ImplementingClass implements AwesomeCalculator{


	public int getSum(int augend, int addend)
	{
		System.out.print("Get Sum Method:\n\t"+augend+" + " + addend + " = ");
		return augend+addend;
	}

	public int getSum(int... summands)
	{
		System.out.println("Adding Multiple numbers\t");		
		int x =0;
		int sum =0;
		for(int i : summands)
		{	
			sum += i;
			if(x < (summands.length-1))
			System.out.print(summands[x]+" + ");
			if (x == (summands.length-1))
			System.out.print(summands[x]+" = ");		
			x++;		
		}	
		return sum;
	}
	
	public double getDifference(double minuend, double subtrahend)
	{
		System.out.print("Get Difference Method:\n\t"+minuend+" - " + subtrahend + " = ");
		return minuend - subtrahend;
	}
		
	public double getProduct(double multiplicand, double multiplier)
	{
		System.out.print("Get Product Method:\n\t"+multiplicand+" * " + multiplier + " = ");
		return multiplicand*multiplier;
	}
	
	public double getProduct(double... factors)
	{
		System.out.println("Multiplying Multiple numbers\t");
//Set as 1 to not multiply by zero in starting of loop
		double product =1;
		int x=0;
		for(double i : factors)
		{
			product *= i; 
			if(x < factors.length-1)
			System.out.print(factors[x]+" * ");
			if(x == factors.length-1)
			System.out.print(factors[x]+" = ");	
		x++;
		}

		return product;
	}
	
	public String getQuotientAndRemainder(double dividend, double divisor)
	{
		System.out.print("Get Quotient and Remainder Method:\n\t"+dividend+" divided by " + divisor + " = ");	
		return (int) (dividend/divisor) + " remainder " + dividend%divisor;
		
	}


	public double toCelsius(double fahrenheit)
	{
		System.out.print("Farenheit to Celcius Method\n\t"+fahrenheit + "\u00b0F is ");	
		return (fahrenheit-32) * 5/9;
	}


	public double toFahrenheit(double celsius)
	{
		System.out.print("Celcius to Farenheit Method\n\t"+celsius + "\u00b0C is ");	
		return celsius * 9/5 + 32;
	}
	
	public double toKilogram(double lbs)
	{
		System.out.print("Pound to Kilogram Method\n\t"+lbs+" lbs is ");
		return lbs*0.45359237;
	}
	
	public double toPound(double kg)
	{
		System.out.print("Kilogram to Pound Method\n\t"+kg+" kg is ");
		return kg*2.20462262185;		
	}
	

	public boolean isPalindrome(String str)
	{
		System.out.println("Palindrome Checker Method");
		System.out.print("\tIs this word a palindrome? '" + str+ "'\n\tAnswer: ");
		//To disregard character casing
		str = str.toLowerCase();		
		Boolean ans = true;
		for(int x=0, y=str.length(); x<str.length(); x++)
		{
			y--;
		//Once any character does not match it automatically tags it as a not palindrome
			if(str.charAt(x) != str.charAt(y))
				ans = false;
			
			}
		return ans;
		
		
	}
	
	
	
}
